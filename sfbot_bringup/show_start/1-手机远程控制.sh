#!/bin/bash
source ~/robot_ws/devel/setup.bash
ROS_MASTER_URI=http://10.0.0.30:11311
gnome-terminal -t "启动lidar" -x bash -c "roslaunch sfbot_bringup rplidar.launch ;exec bash;"
sleep 1
gnome-terminal -t "启动usb摄像头" -x bash -c "roslaunch sfbot_bringup usb_cam.launch ;exec bash;"
sleep 1
gnome-terminal -t "启动底盘" -x bash -c "roslaunch sfbot_bringup base_start.launch ;exec bash;"
sleep 3
gnome-terminal -t "启动Gmapping建图" -x bash -c "roslaunch sfbot_slam gmapping_demo.launch ;exec bash;"
sleep 1
gnome-terminal -t "启动movabase" -x bash -c "roslaunch sfbot_nav move_base_teb.launch ;exec bash;"
