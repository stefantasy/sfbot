#!/bin/bash
killall gnome-terminal-server 
source ~/robot_ws/devel/setup.bash
gnome-terminal -t "启动底盘和雷达" -x bash -c "roslaunch sfbot_bringup lidar_start.launch ;exec bash;"
sleep 3
gnome-terminal -t "启动Gmapping建图" -x bash -c "roslaunch sfbot_slam gmapping_demo.launch ;exec bash;"
sleep 1
gnome-terminal -t "启动movabase" -x bash -c "roslaunch sfbot_nav move_base_teb.launch ;exec bash;"
sleep 3
gnome-terminal -t "启动建图RVIZ" -x bash -c "roslaunch sfbot_rviz gmapping_view.launch ;exec bash;"	
